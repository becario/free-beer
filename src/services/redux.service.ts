import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';

import { IAppState } from '../app/store';
import {BeerModel} from '../models/beer.model';

@Injectable()
export class ReduxService {

  constructor(
    public ngRedux: NgRedux<IAppState>
  ) {}

  public setReduxStateValue(value: any, action: string): void {
    this.ngRedux.dispatch({
      type: action,
      body: value
    });
  }

  public getCurrentBeer(): BeerModel {
    return this.ngRedux.getState().beerDetail;
  }

}

