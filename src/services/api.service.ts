import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/take'

import { BeerModel } from '../models/beer.model';

@Injectable()
export class ApiService {

  private url: string;

  constructor(private http: HttpClient) {
    this.url = 'https://api.punkapi.com/v2/';
  }

  public list(quantity: number, term: string): Promise<BeerModel[]> {
    const endpoint = 'beers?beer_name=' + encodeURI(term) + '&page=1&per_page=' + quantity;

    return new Promise(
      (resolve, reject) => {
        this.http.get(this.url + endpoint).take(1).subscribe(
          (response: BeerModel[]) => {
            resolve(response);
          },
          (error) => {
            reject(error);
          }
        );
      }
    );
  }

  public listCached(quantity: number, term: string): BeerModel[] {
    const storedBeers: BeerModel[] = this.getCachedBeers();
    const searchBeers: BeerModel[] = storedBeers.filter(
      (beer: BeerModel) => {
        let termMatch = true;

        for (let charIndex = 0; charIndex < term.length; charIndex ++) {
          if (beer.name.indexOf(term[charIndex]) === -1) {
            termMatch = false;
          }
        }

        return termMatch;
      }
    );

    return searchBeers;
  }

  public getBeerById(id: number): Promise<BeerModel> {
    const endpoint = 'beers?ids=' + id;

    return new Promise(
      (resolve, reject) => {
        this.http.get(this.url + endpoint).take(1).subscribe(
          (response: BeerModel[]) => {
            if (response[0]) {
              resolve(response[0]);
            } else {
              reject('Reference not found.');
            }
          },
          (error) => {
            reject(error);
          }
        );
      }
    );
  }

  public getCachedBeerById(id: number): BeerModel {
    const storedBeers: BeerModel[] = this.getCachedBeers();

    return storedBeers.filter((beer) => beer.id === id)[0];
  }

  private getCachedBeers(): BeerModel[] {
    return JSON.parse(localStorage.getItem('cachedBeers')) as BeerModel[] || [];
  }

}
