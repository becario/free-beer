import { BeerModel } from '../models/beer.model';
import { BeerListStatusModel } from '../models/beer-list-status.model';

export interface IAppState {
  beerDetail: BeerModel,
  lastSearch: BeerListStatusModel
}

export const INITIAL_STATE: IAppState = {
  beerDetail: null,
  lastSearch: new BeerListStatusModel({items: [], term: ''})
};

export function rootReducer(state: IAppState, action): IAppState {

  switch (action.type) {
    case 'ADD_BEER':
      state.beerDetail = action.body;

      return state;
    case 'SET_SEARCH_STATE':
      state.lastSearch = action.body;
  }

  return state;

}
