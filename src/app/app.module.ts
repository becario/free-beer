import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {
  RouterModule,
  Routes
} from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { ReduxModule } from './redux.module';
import { ListBeersComponent } from '../components/list-beers/list-beers.component';
import { ListBeersModule } from '../components/list-beers/list-beers.module';
import { FooterComponent } from '../components/footer/footer.component';
import { DetailBeerComponent } from '../components/detail-beer/detail-beer.component';
import { ReduxService } from '../services/redux.service';
import {DetailBeerModule} from '../components/detail-beer/detail-beer.module';

const appRoutes: Routes = [
  {
    path: '',
    component: ListBeersComponent,
  },
  {
    path: 'beer/:id',
    component: DetailBeerComponent,
  }
];

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    ReduxModule,
    ListBeersModule,
    DetailBeerModule
  ],
  providers: [ReduxService],
  bootstrap: [AppComponent]
})

export class AppModule { }
