import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  OnDestroy
} from '@angular/core';
import { Router } from '@angular/router';
import { NgRedux } from '@angular-redux/store';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';

import { IAppState } from '../../../app/store';
import { ApiService } from '../../../services/api.service';
import { BeerListStatusModel } from '../../../models/beer-list-status.model';

@Component({
  selector: 'beer-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit, OnDestroy {

  @Output() private result: EventEmitter<BeerListStatusModel>;
  private searchChanged: Subject<string>;

  constructor(
    public ngRedux: NgRedux<IAppState>,
    private router: Router,
    private apiService: ApiService
  ) {
    this.result = new EventEmitter();
    this.searchChanged = new Subject();
  }

  public ngOnInit(): void {
    this.newSearchListener();
  }

  public ngOnDestroy(): void {
    this.searchChanged.unsubscribe();
  }

  public newSearch(newTerm): void {
    if (newTerm.length) {
      this.apiService.list(25, newTerm).then(
        (res) => {
          const listStatus = new BeerListStatusModel({items: res, term: newTerm});
          this.result.emit(listStatus);
        },
        () => {
          const res = this.apiService.listCached(25, newTerm);
          const listStatus = new BeerListStatusModel({items: res, term: newTerm});
          this.result.emit(listStatus);
        }
      );
    } else {
      const listStatus = new BeerListStatusModel({items: [], term: ''});
      this.result.emit(listStatus);
    }
  }

  public modelChanged(text: string): void {
    this.searchChanged.next(text);
  }

  private newSearchListener(): void {
    this.searchChanged.debounceTime(500).subscribe(
      (term: string) => {
        this.newSearch(term);
      }
    );
  }
}
