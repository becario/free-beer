import {
  Component,
  OnInit
} from '@angular/core';
import { Router } from '@angular/router';

import { BeerModel } from '../../models/beer.model';
import { ReduxService } from '../../services/redux.service';
import { BeerListStatusModel } from '../../models/beer-list-status.model';
import { ListBeersService } from './list-beers.service';

@Component({
  selector: 'beer-list',
  templateUrl: './list-beers.component.html',
  styleUrls: ['./list-beers.component.scss']
})
export class ListBeersComponent implements OnInit {

  public beerList: BeerModel[];

  constructor(
    private router: Router,
    private reduxService: ReduxService,
    private listBeersService: ListBeersService
  ) {
    this.beerList = [];
  }

  public ngOnInit(): void {
    this.beerList = this.reduxService.ngRedux.getState().lastSearch.items;
  }

  public getResults(result: BeerListStatusModel): void {
    this.beerList = result.items;
    this.reduxService.setReduxStateValue(result, 'SET_SEARCH_STATE');
    this.listBeersService.updateCache(result.items);
  }

  public goBeerDetail(beer: BeerModel): void {
    this.reduxService.setReduxStateValue(beer, 'ADD_BEER');
    this.router.navigate(['/beer', beer.id]);
  }
}
