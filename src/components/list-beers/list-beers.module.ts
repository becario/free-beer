import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { ApiService } from '../../services/api.service';
import { ListBeersComponent } from './list-beers.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { ListBeersService } from './list-beers.service';

@NgModule({
  declarations: [
    ListBeersComponent,
    TopBarComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    HttpClientModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  providers: [
    ApiService,
    ListBeersService
  ]
})
export class ListBeersModule { }
