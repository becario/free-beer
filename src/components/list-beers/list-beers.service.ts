import { Injectable } from '@angular/core';
import { BeerModel } from '../../models/beer.model';

@Injectable()
export class ListBeersService {

  public updateCache(items: BeerModel[]): void {
    const storedBeers = JSON.parse(localStorage.getItem('cachedBeers')) as BeerModel[] || [];
    const newBeers = items.filter(
      (beer: BeerModel) => {
        let isNew = true;

        storedBeers.forEach(
          (storedBeer: BeerModel) => {
            if (storedBeer.id === beer.id) {
              isNew = false;
            }
          }
        );

        return isNew;
      }
    );
    const currentBeers = [...storedBeers, ...newBeers];

    localStorage.setItem('cachedBeers', JSON.stringify(currentBeers));
  }

}
