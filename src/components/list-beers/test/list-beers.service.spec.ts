import { ListBeersService } from '../list-beers.service';
import { BeerMock } from './beer-item.mock';
import { BeerModel } from '../../../models/beer.model';

describe('ListBeersService', () => {

  let listBeersService: ListBeersService;
  let store: any;

  beforeEach(() => {
    store = {
      cachedBeers: []
    };

    spyOn(localStorage, 'getItem').and.callFake(function (key) {
      return JSON.stringify(store[key]);
    });
    spyOn(localStorage, 'setItem').and.callFake(function (key, value) {
      return JSON.stringify(store[key] = value + '');
    });

    listBeersService = new ListBeersService();
  });

  it('Should create ListBeersService instance.', () => {
    expect(listBeersService).toBeTruthy();
  });

  it('#updateCache', () => {
    const beer = new BeerMock().data as BeerModel;

    listBeersService.updateCache([beer]);
    expect(JSON.parse(JSON.parse(localStorage.getItem('cachedBeers'))).length).toEqual(1);
  });
});
