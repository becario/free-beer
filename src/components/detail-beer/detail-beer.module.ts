import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';

import { DetailBeerComponent } from './detail-beer.component';
import { DetailBeerService } from './detail-beer.service';

@NgModule({
  declarations: [
    DetailBeerComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule
  ],
  providers: [DetailBeerService]
})
export class DetailBeerModule { }
