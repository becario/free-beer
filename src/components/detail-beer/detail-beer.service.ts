import { Injectable } from '@angular/core';

import { BeerModel } from '../../models/beer.model';

@Injectable()
export class DetailBeerService {

  public parseBeerProps(beer: BeerModel, exclude: string[]): any[] {
    let props: any[] = Object.entries(beer).filter(
      (prop) => {
        let valid = true;

        exclude.forEach(
          (excludedKey: string) => {
            if (excludedKey == prop[0]) {
              valid = false;
            }
          }
        );

        if (prop[1] == null) {
          valid = false;
        }


        return valid;
      }
    );

    props = props.map(
      (prop) => {
        if (typeof prop[1] === 'object') {
          return [prop[0], JSON.stringify(prop[1])];
        } else {
          return prop;
        }
      }
    );

    return props;
  }
}
