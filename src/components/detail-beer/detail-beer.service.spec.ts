import { DetailBeerService } from './detail-beer.service';
import { BeerMock } from '../list-beers/test/beer-item.mock';
import { BeerModel } from '../../models/beer.model';

describe('DetailBeerService', () => {

  let detailBeerService: DetailBeerService;

  beforeEach(() => {
    detailBeerService = new DetailBeerService();
  });

  it('Should create ListBeersService instance.', () => {
    expect(detailBeerService).toBeTruthy();
  });

  it('#parseBeerProps', () => {
    const beer = new BeerMock().data as BeerModel;
    const acceptedData: any = detailBeerService.parseBeerProps(beer, ['name']);
    let propExists = false;

    acceptedData.forEach(
      (prop: any[]) => {
        if (prop[0] === propExists) {
          propExists = true;
        }
      }
    );

    expect(propExists).toBeFalsy();
  });
});
