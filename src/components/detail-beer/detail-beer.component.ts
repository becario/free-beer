import {
  Component,
  OnInit
} from '@angular/core';
import {
  ActivatedRoute,
  Router
} from '@angular/router';

import { BeerModel } from '../../models/beer.model';
import { ReduxService } from '../../services/redux.service';
import { ApiService } from '../../services/api.service';
import { DetailBeerService } from './detail-beer.service';

@Component({
  selector: 'beer-detail-beer',
  templateUrl: './detail-beer.component.html',
  styleUrls: ['./detail-beer.component.scss']
})

export class DetailBeerComponent implements OnInit {

  public beer: BeerModel;
  public beerProps: any[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private reduxService: ReduxService,
    private router: Router,
    private apiService: ApiService,
    private detailBeerService: DetailBeerService
  ) {}

  public ngOnInit(): void {
    const beerId: number = parseInt(this.activatedRoute.snapshot.params.id);
    this.initBeer(beerId).then(
      (beer: BeerModel) => {
        this.beer = beer;
        this.reduxService.setReduxStateValue(beer, 'ADD_BEER');
        this.beerProps = this.detailBeerService.parseBeerProps(beer, ['id', 'name', 'tagline', 'image_url']);
      },
      (error) => {
        alert(error);
        this.goHome();
      }
    );
  }

  public goHome(): void {
    this.router.navigate(['/']);
  }

  private initBeer(id: number): Promise<BeerModel> {
    return new Promise(
      (resolve, reject) => {
        // 1st attempt: get beer from redux store.
        let beer = this.reduxService.getCurrentBeer();

        if (beer) {
          resolve(beer)
        } else {
          // 2nd attempt: get beer from API.
          this.apiService.getBeerById(id).then(
            (beer: BeerModel) => {
              resolve(beer);
            },
            (error) => {
              // 3nd attempt: get beer from local storage.
              const cachedBeer = this.apiService.getCachedBeerById(id);

              if (cachedBeer) {
                resolve(cachedBeer);
              } else {
                // Could not find beer.
                console.log(error);
                reject(error.message);
              }
            }
          )
        }
      }
    )

  }

}
