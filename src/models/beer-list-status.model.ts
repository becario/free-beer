import { BeerModel } from './beer.model';

export class BeerListStatusModel {
  public items: BeerModel[];
  public term: string;

  constructor(args?: any) {
    Object.assign(this, ...args);
  }
}
