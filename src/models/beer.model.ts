import { ValueModel } from './value.model';

export class BeerModel {
  public id: number;
  public name: string;
  public tagline: string;
  public first_brewed: string;
  public description: string;
  public image_url: string;
  public abv: number;
  public ibu: number;
  public target_fg: number;
  public target_og: number;
  public ebc: number;
  public srm: number;
  public ph: number;
  public attenuation_level: number;
  public volume: ValueModel;
  public boil_volume: ValueModel;
  public method: any;
  public ingredients: any;
  public food_pairing: string[];
  public brewers_tips: string;
  public contributed_by: string;

  constructor(args?: any) {
    Object.assign(this, ...args);
  }
}
