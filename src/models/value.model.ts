export class ValueModel {
  public value: number;
  public unit: string;

  constructor(args?: any) {
    Object.assign(this, ...args);
  }
}
